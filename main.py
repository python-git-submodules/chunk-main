# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from library.chunk_library.create_chunk import ChunkCreator

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    cc = ChunkCreator()
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    chunks = cc.create_chunk(numbers, 4)
    print(chunks)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
