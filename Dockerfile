FROM python:3.8-slim-buster

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
RUN rm -rf .idea
RUN rm -rf library/.idea library/Pipfile library/main.py

CMD ["python", "main.py"]