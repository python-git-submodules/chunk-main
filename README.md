## Build Image

```shell
docker build --tag chunk-main-0.1
```

## Run Image

```shell
docker run chunk-main-0.1:latest
```

## Export Image

```sh
 docker export -o chunk-docker-new.tar
```